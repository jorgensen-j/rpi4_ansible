#!/bin/bash

# ensure brltty is removed 

esptool.py --chip esp32 --port /dev/ttyUSB0 erase_flash

esptool.py --chip esp32 --port /dev/ttyUSB0 --baud 460800 write_flash -z 0x1000 esp32-20190125-v1.10.bin

# to connect
#picocom /dev/ttyUSB0 -b115200
