#!/bin/bash

# ensure brltty is removed 

esptool.py --port /dev/ttyUSB0 erase_flash

esptool.py --port /dev/ttyUSB0 --baud 460800 write_flash --flash_size=detect 0 esp8266-20220618-v1.19.1.bin

# to connect
#picocom /dev/ttyUSB0 -b115200
