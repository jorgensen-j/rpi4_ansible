# Rpi4_Ansible

To run first install git and ansible

`sudo apt update && sudo apt install git ansible`

Then run

`ansible-pull -o -U https://gitlab.com/jorgensen-j/rpi4_ansible.git main.yml --checkout main`